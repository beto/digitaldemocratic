#!make
include digitaldemocratic.conf
export $(shell sed 's/=.*//' digitaldemocratic.conf)

VERSION := 0.0.1-rc0
export VERSION

CUSTOM_PATH=$(shell pwd)

.PHONY: environment
environment:
	git submodule update --init --recursive
	#cp custom/menu/* isard-sso/docker/api/src/
	#cp custom/img/* isard-sso/docker/api/src/api/static/img/
	#cp custom/login/logo.png isard-sso/docker/keycloak/themes/liiibrelite/login/resources/img/logo.png
	#cp custom/login/background.png isard-sso/docker/keycloak/themes/liiibrelite/login/resources/img/loginBG.png
	#cp custom/login/style.css isard-sso/docker/keycloak/themes/liiibrelite/login/resources/css/
	mkdir -p custom/system/keycloak
	cp -R isard-sso/docker/keycloak/themes custom/system/keycloak/
	cp custom/login/logo.png custom/system/keycloak/themes/liiibrelite/login/resources/img/logo.png
	cp custom/login/background.png custom/system/keycloak/themes/liiibrelite/login/resources/img/loginBG.png
	cp custom/login/style.css custom/system/keycloak/themes/liiibrelite/login/resources/css/
	cp digitaldemocratic.conf isard-apps/.env
	echo "CUSTOM_PATH=$(CUSTOM_PATH)" >> isard-apps/.env
	echo "BUILD_ROOT_PATH=$(CUSTOM_PATH)/isard-apps" >> isard-apps/.env
	cp isard-apps/.env isard-apps/docker/postgresql && \
	cp isard-apps/.env isard-apps/docker/mariadb && \
	cp isard-apps/.env isard-apps/docker/moodle && \
	cp isard-apps/.env isard-apps/docker/nextcloud && \
	cp isard-apps/.env isard-apps/docker/wordpress && \
	cp isard-apps/.env isard-apps/docker/etherpad

	cp digitaldemocratic.conf isard-sso/docker-compose-parts/.env
	echo "CUSTOM_PATH=$(CUSTOM_PATH)" >> isard-sso/docker-compose-parts/.env
	echo "BUILD_ROOT_PATH=$(CUSTOM_PATH)/isard-sso" >> isard-sso/docker-compose-parts/.env

	cp digitaldemocratic.conf docker-compose-parts/.env
	echo "BUILD_ROOT_PATH=$(CUSTOM_PATH)" >> docker-compose-parts/.env

.PHONY: build
build: environment
	echo BUILD_ROOT_PATH=$(CUSTOM_PATH)/isard-sso > .env
	docker-compose 	-f docker-compose-parts/backup.yml \
					-f docker-compose-parts/admin.yml \
					config > dd.yml
	echo CUSTOM_PATH=$(CUSTOM_PATH) > .env
	echo BUILD_ROOT_PATH=$(CUSTOM_PATH)/isard-sso >> .env
	docker-compose 	-f isard-sso/docker-compose-parts/haproxy.yml \
					-f isard-sso/docker-compose-parts/api.yml \
					-f isard-sso/docker-compose-parts/keycloak.yml \
					-f isard-sso/docker-compose-parts/avatars.yml \
					-f isard-apps/docker/postgresql/postgresql.yml \
					config > sso.yml
					#-f isard-sso/docker-compose-parts/freeipa.yml 
	echo BUILD_ROOT_PATH=$(CUSTOM_PATH)/isard-apps > .env
	docker-compose 	-f isard-apps/docker/moodle/moodle.yml \
					-f isard-apps/docker/nextcloud/nextcloud.yml \
					-f isard-apps/docker/wordpress/wordpress.yml \
					-f isard-apps/docker/etherpad/etherpad.yml \
					-f isard-apps/docker/onlyoffice/onlyoffice.yml \
					-f isard-apps/docker/redis/redis.yml \
					-f isard-apps/docker/postgresql/postgresql.yml \
					-f isard-apps/docker/mariadb/mariadb.yml \
					-f isard-apps/docker/network.yml \
					config > apps.yml
	docker-compose -f dd.yml -f sso.yml -f apps.yml config > docker-compose.yml
	rm dd.yml sso.yml apps.yml
	docker-compose build

.PHONY: up
up: build
	docker-compose up -d --no-deps

.PHONY: down
down:
	docker-compose down

.PHONY: remove
remove: down
	rm -rf /opt/digitaldemocratic/postgres
	rm -rf /opt/digitaldemocratic/redis
	rm -rf /opt/digitaldemocratic/wordpress
	rm -rf /opt/digitaldemocratic/nextcloud
	rm -rf /opt/digitaldemocratic/mariadb
	rm -rf /opt/digitaldemocratic/freeipa
	# Leaves haproxy folder with certificates. Remove manually to get new certificates.

.PHONY: add-plugins
add-plugins:
	# Nextcloud
	#cp -R $$BUILD_ROOT_PATH/isard-apps/docker/nextcloud/themes/* $$DATA_FOLDER/nextcloud/themes/
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set default_language --value="ca"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set skeletondirectory --value=''
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:disable firstrunwizard
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:disable recommendations
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:install forms
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:install polls
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:install calendar
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set theme  --value=digitaldemocratic
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set allow_local_remote_servers  --value=true
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings maintenance:theme:update
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:disable dashboard'"

	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install mail'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable mail'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable user_saml'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install ownpad'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "/ownpad_cfg.sh"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable ownpad'"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set ownpad ownpad_etherpad_enable --value="yes"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set ownpad ownpad_etherpad_host --value="https://pad.$$DOMAIN"

	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:install onlyoffice
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice DocumentServerUrl --value="https://oof.$$DOMAIN"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice jwt_secret --value="secret"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice jwt_header --value="Authorization"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set allow_local_remote_servers  --value=true

# Allow nextcloud into other apps iframes
# Content-Security-Policy: frame-ancestors 'self' *.$$DOMAIN;
	docker exec -ti isard-apps-nextcloud-app sed -ie "/protected \\\$$allowedFrameAncestors = \[/{n;s/\('\\\\\'self\\\\\'\)\('\)/\1 *.$$DOMAIN\2/}" /var/www/html/lib/public/AppFramework/Http/ContentSecurityPolicy.php

	# default labels
	echo "INSERT INTO oc_systemtag (name, editable) VALUES ('nombre etiqueta', 0);" | docker exec -i isard-apps-postgresql psql -U admin -d nextcloud

	# Wordpress
	## Multisite
	docker exec -ti isard-apps-wordpress /bin/sh -c "/multisite.sh"

.PHONY: connect-saml
connect-saml:
	while [ "`docker inspect -f {{.State.Health.Status}} isard-apps-moodle`" != "healthy" ]; do     sleep 2; done
