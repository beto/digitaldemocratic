# SAML2 Plugin development environment (moodle)

NOTE: This could be completely outdated as the current version mounts moodle html source outside the container.

All this have to be done as the image doesn't let html external folder mounted as volume (image doesn't use root)

1. Start isard-apps-moodle docker with default config. Wait for moodle to be ready.
2. Enter docker and copy html to external folder:
   1. docker exec -ti isard-apps-moodle /bin/sh
   2. cd /var/www/html
   3. mkdir /var/www/moodledata/html
   4. cp -R . /var/www/moodledata/html

Now you open two terminals:

- docker exec -ti isard-apps-moodle /bin/sh
- docker logs isard-apps-moodle --follow

You can edit saml2 plugin from host (/opt/isard-office/moodle/data/html/auth/saml2) and copy it to the current html folder:

- /var/www/html/auth/saml2 $ cp -R /var/www/moodledata/html/auth/saml2/* .

When you finish developing get the new plugin code into a zip and in the correct src folder:

- cd ${DATA_FOLDER}/moodle/data/html/auth/ && zip -r <src git path>/isard-office/docker/moodle/plugins/auth_saml2.zip saml2

## SAML2 Plugin src

The modified source files are:

- auth.php (lines 570 to 595, sync_roles call added)
- locallib.php (function sync_roles)

Also the common plugin setup fields and lang strings:

- settings.php (lines 314 to 333)
- lang/en/auth_saml2.php (lines 24 to 29)

# Big Blue Button

TODO:

- Audio fails with docker in iptables=false and managed by firewalld in masquerade mode. This is due to coturn that doesn't like being behind nat.
- Firewalld + BBB: As  BBB will 'take' the host interface we should:
  - Remove /etc/docker/daemon.json the iptables: false
  - firewall-cmd --zone=public --remove-interface=docker0 --permanent
  - firewall-cmd --zone=docker --add-interface=docker0 --permanent
  - Now the docker applies iptables as per container. Note that we don't have control over this from now on.
- Scalelite
- Script creation of base debian with virt-install and then replicate BBBs (partially done)
