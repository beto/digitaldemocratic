# What's this

This project allows to bring up with ease a full identity provider and many apps to have an environment thought for schools and universities. The project will provide an integrated solution to handle the common environment in education:

- Classrooms: A moodle instance with custom theme and custom plugins.
- Cloud drive: A nextcloud instance with custom theme and custom plugins.
- Documents: An onlyoffice instance integrated with nextcloud.
- Web pages: A wordpress instance with custom theme and custom plugins.
- Pad: An etherpad instance integrated with nextcloud.
- Conferences: A BigBlueButton integrated with moodle and nextcloud. This needs an standalone host right now 
- Forms: A forms nextcloud plugins.
- ... (some apps are there like jitsi or BigBlueButton but not fully integrated right now)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Project status

It works but as we are working on it it will have lots of improvements upcoming months. Some automatizations need to be done, specially with SAML integration and Keycloak initial personalization.

Your collaboration is wellcome! Just fork this repo and do a PR or open us an issue.

# DigitalDemocratic Documentation

This documentation is writed in Markdown using [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

See `docs` directory for Markdown files or the [auto-built site](https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Quick start

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Edit digitaldemocratic.conf file variables to suit your needs.

```
cp -R custom.sample custom
```

Edit and replace files to personalize system.

```
make up
```

Wait till apps come up and add plugins:

```
make add-plugins
```
## Extended

You can start this project in any docker & docker-compose host (any OS should work). To install those packages in your distro refer to docker & docker-compose in this documentation and in the sysadm folder you have some scripts.

Any distro should work but, if you want to use our sysadm scripts to install docker & docker compose use Debian Buster (10).

### Clone and submodules

git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive

### docker

Refer to the official documentation (https://docs.docker.com/engine/install/) or use our sysadm script if you are on a Debian Buster (10).

### docker-compose

Refer to the official documentation (https://docs.docker.com/compose/install/) or use our sysadm script if you are on a Debian Buster (10).

### Settings

Copy digitaldemocratic.conf.example to digitaldemocratic.conf and edit to suit your needs. At least (for development) you need to adapt the DOMAIN variable to your root domain.

- PRODUCTION: You need a multidomain dns (or redirect the multiple subdomains) to your host machine.
- DEVELOPMENT: You can just edit your /etc/hosts and add all of them there just for testing purposes.

#### Subdomains
- Keycloak: sso.<yourdomain.org>
- FreeIPA: ipa.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>

### Personalization

Copy recursively the custom.sample folder to custom and edit the menu yaml files and replace images.

### Bring the project up
```
make up
```
Then wait for the apps to come up (test by accessing the multiple subdomains) and execute:
```
make add-plugins
```
### Integration

Please read the SAML_README.md in de docs folder to integrate all apps.
