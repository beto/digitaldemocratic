from requests import get, post
# Module variables to connect to moodle api

class Moodle():
    """https://github.com/mrcinv/moodle_api.py
        https://docs.moodle.org/dev/Web_service_API_functions
        https://docs.moodle.org/311/en/Using_web_services
    """

    def __init__(self, key, url, endpoint):
        self.key = key
        self.url = url
        self.endpoint = endpoint
    
    def rest_api_parameters(self, in_args, prefix='', out_dict=None):
        """Transform dictionary/array structure to a flat dictionary, with key names
        defining the structure.
        Example usage:
        >>> rest_api_parameters({'courses':[{'id':1,'name': 'course1'}]})
        {'courses[0][id]':1,
         'courses[0][name]':'course1'}
        """
        if out_dict==None:
            out_dict = {}
        if not type(in_args) in (list,dict):
            out_dict[prefix] = in_args
            return out_dict
        if prefix == '':
            prefix = prefix + '{0}'
        else:
            prefix = prefix + '[{0}]'
        if type(in_args)==list:
            for idx, item in enumerate(in_args):
                self.rest_api_parameters(item, prefix.format(idx), out_dict)
        elif type(in_args)==dict:
            for key, item in in_args.items():
                self.rest_api_parameters(item, prefix.format(key), out_dict)
        return out_dict
    
    def call(self, fname, **kwargs):
        """Calls moodle API function with function name fname and keyword arguments.
        Example:
        >>> call_mdl_function('core_course_update_courses',
                               courses = [{'id': 1, 'fullname': 'My favorite course'}])
        """
        parameters = self.rest_api_parameters(kwargs)
        parameters.update({"wstoken": self.key, 'moodlewsrestformat': 'json', "wsfunction": fname})
        response = post(self.url+self.endpoint, parameters, verify=False)
        response = response.json()
        if type(response) == dict and response.get('exception'):
            raise SystemError("Error calling Moodle API\n", response)
        return response

    def create_user(self, email, username, password, first_name='-', last_name='-'):
        data = [{'username': username, 'email':email, 
                 'password': password, 'firstname':first_name, 'lastname':last_name}]
        user = self.call('core_user_create_users', users=data)
        return user

    def get_user_by(self, key, value):
        criteria = [{'key': key, 'value': value}]
        user = self.call('core_user_get_users', criteria=criteria)
        return user
    
    def enroll_user_to_course(self, user_id, course_id, role_id=5):
        # 5 is student
        data = [{'roleid': role_id, 'userid': user_id,  'courseid': course_id}]
        enrolment = self.call('enrol_manual_enrol_users', enrolments=data)
        return enrolment
        
    def get_quiz_attempt(self, quiz_id, user_id):
        attempts = self.call('mod_quiz_get_user_attempts', quizid=quiz_id, userid=user_id)
        return attempts
    
    def get_cohorts(self):
        cohorts = self.call('core_cohort_get_cohorts')
        return cohorts