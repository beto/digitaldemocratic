#!/usr/bin/env python
# coding=utf-8

from admin import app

import os, sys
import logging as log
import traceback

class loadConfig():

    def __init__(self, app=None):  
        try:
            app.config.setdefault('DOMAIN', os.environ['DOMAIN'])

        except Exception as e:
            log.error(traceback.format_exc())
            raise
