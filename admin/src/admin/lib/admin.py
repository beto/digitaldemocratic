from .keycloak import Keycloak
from .moodle import Moodle
from .nextcloud import Nextcloud

from pprint import pprint

class Admin():
    def __init__(self):
        URL="http://isard-sso-keycloak:8080/auth/"
        USERNAME='admin'
        PASSWORD='keycloakkeycloak'
        REALM="master"
        VERIFY_CERT=True
        self.keycloak=Keycloak(URL,USERNAME,PASSWORD,REALM,VERIFY_CERT)
        
        KEY = "a1d8b3248e1a3e3b839645503df0e3a5"
        URL = "https://moodle.santantoni.duckdns.org"
        ENDPOINT="/webservice/rest/server.php"
        self.moodle=Moodle(KEY,URL,ENDPOINT)

        URL="https://nextcloud.santantoni.duckdns.org"
        USERNAME='admin'
        PASSWORD='N3xtcl0ud'
        VERIFY_CERT=False
        self.nextcloud=Nextcloud(URL,USERNAME,PASSWORD,VERIFY_CERT)

        pprint(self.get_moodle_groups())
        # pprint(self.get_moodle_users())
        # pprint(self.get_keycloak_users())
        # pprint(self.get_nextcloud_users())

    def get_moodle_users(self):
        users = self.moodle.get_user_by('email','%%')['users']
        return [{"id":u['id'],
                "username":u['username'],
                "first": u['firstname'], 
                "last": u['lastname'], 
                "email": u['email']} 
                for u in users]

    def get_keycloak_users(self):
        users = self.keycloak.get_users()
        return [{"id":u['id'],
                "username":u['username'],
                "first": u.get('firstName',None), 
                "last": u.get('lastName',None), 
                "email": u.get('email','')} 
                for u in users]

    def get_nextcloud_users(self):
        users = self.nextcloud.get_users_list()
        users_list=[]
        for user in users:
            u=self.nextcloud.get_user(user)
            users_list.append({"id":u['id'],
                            "username":u['id'],
                            "first": u['displayname'], 
                            "last": None, 
                            "email": u['email']})
        return users_list

    def get_mix_users(self):
        kusers=self.get_keycloak_users()
        musers=self.get_moodle_users()
        nusers=self.get_nextcloud_users()

        kusers_usernames=[u['username'] for u in kusers]
        musers_usernames=[u['username'] for u in musers]
        nusers_usernames=[u['username'] for u in nusers]

        all_users_usernames=set(kusers_usernames+musers_usernames+nusers_usernames)

        users=[]
        for username in all_users_usernames:
            theuser={}
            keycloak_exists=[u for u in kusers if u['username'] == username]
            if len(keycloak_exists):
                theuser=keycloak_exists[0]
                theuser['keycloak']=True
            else:
                theuser['id']=False
                theuser['keycloak']=False

            moodle_exists=[u for u in musers if u['username'] == username]
            if len(moodle_exists):
                theuser={**moodle_exists[0], **theuser}
                theuser['moodle']=True
            else:
                theuser['moodle']=False

            nextcloud_exists=[u for u in nusers if u['username'] == username]
            if len(nextcloud_exists):
                theuser={**nextcloud_exists[0], **theuser}
                theuser['nextcloud']=True
            else:
                theuser['nextcloud']=False

            users.append(theuser)
        
        return users

    def get_roles(self):
        return self.keycloak.get_roles()

    def get_keycloak_groups(self):
        return self.keycloak.get_groups()

    def get_moodle_groups(self):
        return self.moodle.get_cohorts()

    def get_nextcloud_groups(self):
        return self.nextcloud.get_groups_list()

    def get_groups(self):
        kgroups=self.get_keycloak_groups()
        mgroups=self.get_moodle_groups()
        ngroups=self.get_nextcloud_groups()

        kgroups_names=[g['name'] for g in kgroups]
        mgroups_names=[g['name'] for g in mgroups]
        ngroups_names=ngroups

        all_groups_names=set(kgroups_names+mgroups_names+ngroups_names)

        groups=[]
        for name in all_groups_names:
            thegroup={}
            keycloak_exists=[g for g in kgroups if g['name'] == name]
            if len(keycloak_exists):
                thegroup=keycloak_exists[0]
                thegroup['keycloak']=True
            else:
                thegroup['id']=False
                thegroup['keycloak']=False

            moodle_exists=[g for g in mgroups if g['name'] == name]
            if len(moodle_exists):
                thegroup['path']=''
                thegroup={**moodle_exists[0], **thegroup}
                thegroup['moodle']=True
            else:
                thegroup['moodle']=False

            nextcloud_exists=[g for g in ngroups if g == name] 
            if len(nextcloud_exists):
                nextcloud={"id":nextcloud_exists[0],
                            "name":nextcloud_exists[0],
                            "path":''}
                thegroup={**nextcloud, **thegroup}
                thegroup['nextcloud']=True
            else:
                thegroup['nextcloud']=False

            groups.append(thegroup)
        
        return groups