#!/usr/bin/env python
# coding=utf-8
import time
from admin import app as application
from datetime import datetime, timedelta
import pprint

import logging
import traceback
import yaml, json

from jinja2 import Environment, FileSystemLoader

from keycloak import KeycloakAdmin

class Keycloak():
    """https://www.keycloak.org/docs-api/13.0/rest-api/index.html
        https://github.com/marcospereirampj/python-keycloak
        https://gist.github.com/kaqfa/99829941121188d7cef8271f93f52f1f
    """
    def __init__(self,url,username,password,realm,verify=True):
        self.keycloak_admin = KeycloakAdmin(server_url=url,
                                    username=username,
                                    password=password,
                                    realm_name=realm,
                                    verify=verify)

        from pprint import pprint
        ######## Example create group and subgroup
        
        try:
            self.add_group('level1')
        except:
            self.delete_group(self.get_group('/level1')['id'])
        self.add_group('level1')
        self.add_group('level2',parent=self.get_group('/level1')['id'])
        pprint(self.get_groups())

        ######## Example roles
        # try:
        #     self.add_role('superman')
        # except:
        #     self.delete_role('superman')
        # self.add_role('superman')
        pprint(self.get_roles())

    ## USERS

    def get_user_id(self,username):
        return self.keycloak_admin.get_user_id(username)

    def get_users(self):
        return self.keycloak_admin.get_users({})

    def add_user(self,username,first,last,email,password):
        # Returns user id
        return self.keycloak_admin.create_user({"email": email,
                            "username": username,
                            "enabled": True,
                            "firstName": first,
                            "lastName": last,
                            "credentials":[{"type":"password",
                                "value":password,
                                "temporary":False}]})

    def add_user_role(self,client_id,user_id,role_id,role_name):
        return self.keycloak_admin.assign_client_role(client_id="client_id", user_id="user_id", role_id="role_id", role_name="test")

    def delete_user(self,userid):
        return self.keycloak_admin.delete_user(user_id=userid)

    ## SYSTEM
    def get_server_info(self):
        return self.keycloak_admin.get_server_info()
    
    def get_server_clients(self):
        return self.keycloak_admin.get_clients()

    ## GROUPS
    def get_groups(self,with_subgroups=True):
        groups = self.keycloak_admin.get_groups()
        subgroups=[]
        if with_subgroups:
            for group in groups:
                if len(group['subGroups']):
                    for sg in group['subGroups']:
                        subgroups.append(sg)
        import pprint
        return groups+subgroups

    def get_group(self,path,recursive=True):
        return self.keycloak_admin.get_group_by_path(path=path,search_in_subgroups=recursive)

    def add_group(self,name,parent=None):
        return self.keycloak_admin.create_group({"name":name}, parent=parent)

    def delete_group(self,group_id):
        return self.keycloak_admin.delete_group(group_id=group_id)

    ## ROLES
    def get_roles(self):
        return self.keycloak_admin.get_realm_roles()

    def get_role(self,name):
        return self.keycloak_admin.get_realm_role(name=name)

    def add_role(self,name):
        return self.keycloak_admin.create_realm_role({"name":name})

    def delete_role(self,name):
        return self.keycloak_admin.delete_realm_role(name)


    ## CLIENTS

    def get_client_roles(self,client_id):
        return self.keycloak_admin.get_client_roles(client_id=client_id)

    # def add_client_role(self,client_id,roleName):
    #     return self.keycloak_admin.create_client_role(client_id=client_id, {'name': roleName, 'clientRole': True})
