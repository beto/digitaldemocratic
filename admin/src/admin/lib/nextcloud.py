#!/usr/bin/env python
# coding=utf-8

#from ..lib.log import *
import time,requests,json,pprint
import traceback
import logging as log
from .nextcloud_exc import *

class Nextcloud():
    def __init__(self,url,username,password,verify):
        self.verify_cert=verify
        self.apiurl=url+'/ocs/v1.php/cloud/'
        self.shareurl=url+'/ocs/v2.php/apps/files_sharing/api/v1/'
        self.davurl=url+'/remote.php/dav/files/'
        self.auth=(username,password)
        self.user=username

    def _request(self,method,url,data={},headers={'OCS-APIRequest':'true'},auth=False):
        if auth == False: auth=self.auth
        try:
            return requests.request(method, url, data=data, auth=auth, verify=self.verify_cert, headers=headers).text

        ## At least the ProviderSslError is not being catched or not raised correctly
        except requests.exceptions.HTTPError as errh:
            raise ProviderConnError
        except requests.exceptions.Timeout as errt:
            raise ProviderConnTimeout
        except requests.exceptions.SSLError as err:
            raise ProviderSslError
        except requests.exceptions.ConnectionError as errc:
            raise ProviderConnError
        # except requests.exceptions.RequestException as err:
        #     raise ProviderError
        except Exception as e:
            if str(e) == 'an integer is required (got type bytes)':
                raise ProviderConnError
            raise ProviderError

    def check_connection(self):
        url = self.apiurl + "users/"+self.user+"?format=json"
        try:
            result = self._request('GET',url)
            if json.loads(result)['ocs']['meta']['statuscode'] == 100: return  True
            raise ProviderError
        except requests.exceptions.HTTPError as errh:
            raise ProviderConnError
        except requests.exceptions.ConnectionError as errc:
            raise ProviderConnError
        except requests.exceptions.Timeout as errt:
            raise ProviderConnTimeout
        except requests.exceptions.SSLError as err:
            raise ProviderSslError
        except requests.exceptions.RequestException as err:
            raise ProviderError
        except Exception as e:
            if str(e) == 'an integer is required (got type bytes)':
                raise ProviderConnError
            raise ProviderError

    def get_user(self,userid):
        url = self.apiurl + "users/"+userid+"?format=json"
        try:
            result = json.loads(self._request('GET',url))
            if result['ocs']['meta']['statuscode'] == 100: return  result['ocs']['data']
            raise ProviderItemNotExists
        except:
            log.error(traceback.format_exc())
            raise 
        # 100 - successful

    def get_users_list(self):
        url = self.apiurl + "users?format=json"
        try:
            result = json.loads(self._request('GET',url))
            if result['ocs']['meta']['statuscode'] == 100: return result['ocs']['data']['users']
            log.error('Get Nextcloud provider users list error: '+str(result))
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def add_user(self,userid,userpassword,quota,group='',email='',displayname=''):
        data={'userid':userid,'password':userpassword,'quota':quota,'groups[]':group,'email':email,'displayname':displayname}
        url = self.apiurl + "users?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url,data=data,headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 102: raise ProviderItemExists
            if result['ocs']['meta']['statuscode'] == 104: raise ProviderGroupNotExists
            log.error('Get Nextcloud provider user add error: '+str(result))
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - username already exists
            # 103 - unknown error occurred whilst adding the user
            # 104 - group does not exist
            # 105 - insufficient privileges for group
            # 106 - no group specified (required for subadmins)
            # 107 - all errors that contain a hint - for example “Password is among the 1,000,000 most common ones. Please make it unique.” (this code was added in 12.0.6 & 13.0.1)

    def delete_user(self,userid):
        url = self.apiurl + "users/"+userid+"?format=json"
        try:
            result = json.loads(self._request('DELETE',url))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 101: raise ProviderUserNotExists
            log.error(traceback.format_exc())
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - failure

    def enable_user(self,userid):
        None

    def disable_user(self,userid):
        None

    def exists_user_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.davurl + userid +"/" + folder+"?format=json"
        headers = {
            'Depth': '0',
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = self._request('PROPFIND',url,auth=auth,headers=headers)
            if '<d:status>HTTP/1.1 200 OK</d:status>' in result: return True
            return False
        except:
            log.error(traceback.format_exc())
            raise 

    def add_user_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.davurl + userid +"/" + folder+"?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = self._request('MKCOL',url,auth=auth,headers=headers)
            if result=='': return True
            if '<s:message>The resource you tried to create already exists</s:message>' in result: raise ProviderItemExists
            log.error(result.split('message>')[1].split('<')[0])
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def exists_user_share_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        url = self.shareurl + "shares?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result =  json.loads(self._request('GET', url, auth=auth, headers=headers))
            if result['ocs']['meta']['statuscode']==200:
                share=[s for s in result['ocs']['data'] if s['path'] == '/'+folder]
                if len(share) >= 1:
                    # Should we delete all but the first (0) one?
                    return {'token': share[0]['token'],
                            'url': share[0]['url']}
                raise ProviderItemNotExists
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def add_user_share_folder(self,userid,userpassword,folder='IsardVDI'):
        auth=(userid,userpassword)
        data={'path':'/'+folder,'shareType':3}
        url = self.shareurl + "shares?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url, data=data, auth=auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100 or result['ocs']['meta']['statuscode'] == 200:
                return {'token': result['ocs']['data']['token'],
                        'url': result['ocs']['data']['url']}
            log.error('Add user share folder error: '+result['ocs']['meta']['message'])
            raise ProviderFolderNotExists
        except:
            log.error(traceback.format_exc())
            raise 

    def get_group(self,userid):
        None

    def get_groups_list(self):
        url = self.apiurl + "groups?format=json"
        try:
            result = json.loads(self._request('GET',url))
            if result['ocs']['meta']['statuscode'] == 100: return  [g for g in result['ocs']['data']['groups']]
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 

    def add_group(self,groupid):
        data={'groupid':groupid} 
        url = self.apiurl + "groups?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('POST',url, data=data, auth=self.auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            if result['ocs']['meta']['statuscode'] == 102: raise ProviderItemExists
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - group already exists
            # 103 - failed to add the group

    def delete_group(self,groupid):
        url = self.apiurl + "groups/"+groupid+"?format=json"
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'OCS-APIRequest': 'true',
        }
        try:
            result = json.loads(self._request('DELETE',url, auth=self.auth, headers=headers))
            if result['ocs']['meta']['statuscode'] == 100: return True
            log.error(traceback.format_exc())
            raise ProviderOpError
        except:
            log.error(traceback.format_exc())
            raise 
            # 100 - successful
            # 101 - invalid input data
            # 102 - group already exists
            # 103 - failed to add the group

