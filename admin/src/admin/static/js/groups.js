
$(document).on('shown.bs.modal', '#modalAddDesktop', function () {
    modal_add_desktops.columns.adjust().draw();
}); 

$(document).ready(function() {

	$('.btn-new').on('click', function () {
            $("#modalAdd")[0].reset();
            $('#modalAddDesktop').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show');
            $('#modalAdd').parsley();
	});

	//DataTable Main renderer
	var table = $('#groups').DataTable({
        "ajax": {
            "url": "/dd-admin/groups_list",
            "dataSrc": ""
        },
        "language": {
            "loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "emptyTable": "<h1>You don't have any group created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
        },           
        "rowId": "id",
        "deferRender": true,
        "columns": [
            {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "width": "10px",
            "defaultContent": '<button class="btn btn-xs btn-info" type="button"  data-placement="top" ><i class="fa fa-plus"></i></button>'
            },
            { "data": "id", "width": "10px" },
            { "data": "keycloak", "width": "10px" },
            { "data": "moodle", "width": "10px" },
            { "data": "nextcloud", "width": "10px" },
            { "data": "name", "width": "10px" },
            { "data": "path", "width": "10px" },
            ],
         "order": [[3, 'asc']],
         "columnDefs": [ {
            "targets": 2,
            "render": function ( data, type, full, meta ) {
              if(full.keycloak){
                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                }else{
                    return '<i class="fa fa-close" style="color:darkred"></i>'
                };
            }},
            {
            "targets": 3,
            "render": function ( data, type, full, meta ) {
                if(full.moodle){
                    return '<i class="fa fa-check" style="color:lightgreen"></i>'
                }else{
                    return '<i class="fa fa-close" style="color:darkred"></i>'
                };
            }},
            {
                "targets": 4,
                "render": function ( data, type, full, meta ) {
                    if(full.nextcloud){
                        return '<i class="fa fa-check" style="color:lightgreen"></i>'
                    }else{
                        return '<i class="fa fa-close" style="color:darkred"></i>'
                    };
                }},
            ] 
} );
})