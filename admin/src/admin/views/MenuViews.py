#!flask/bin/python
# coding=utf-8
from admin import app
import logging as log
import traceback

from uuid import uuid4
import time,json
import sys,os
from flask import render_template, Response, request, redirect, url_for, jsonify

@app.route('/dd-admin/users')
# @login_required
def users():
    return render_template('pages/users.html', title="Users", nav="Users")

@app.route('/dd-admin/users_list')
# @login_required
def users_list():
    return json.dumps(app.admin.get_mix_users()), 200, {'Content-Type': 'application/json'}


@app.route('/dd-admin/roles')
# @login_required
def roles():
    return render_template('pages/roles.html', title="Roles", nav="Roles")

@app.route('/dd-admin/roles_list')
# @login_required
def roles_list():
    return json.dumps(app.admin.get_roles()), 200, {'Content-Type': 'application/json'}


@app.route('/dd-admin/groups')
# @login_required
def groups():
    return render_template('pages/groups.html', title="Groups", nav="Groups")

@app.route('/dd-admin/groups_list')
# @login_required
def groups_list():
    return json.dumps(app.admin.get_groups()), 200, {'Content-Type': 'application/json'}