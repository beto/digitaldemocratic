cp ../.env .
source .env
docker-compose stop isard-apps-wordpress
docker rm isard-apps-wordpress
rm -rf /opt/isard-office/wordpress
echo "DROP DATABASE wordpress;" | docker exec -i isard-apps-postgresql psql -U admin
docker-compose up -d isard-apps-wordpress
docker-compose restart isard-apps-wordress-cli
docker logs isard-apps-wordpress --follow
